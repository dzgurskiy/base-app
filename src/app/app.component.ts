import { Component } from '@angular/core';

import { SettingsService } from './app-package/services/settings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(
    private settingsService: SettingsService
  ) {
    this.settingsService.isShowMiniFabButtons = true;
  }
}
