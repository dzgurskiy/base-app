import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './routing.module';

import { TestRoutingComponent } from './components/test-routing/test-routing.component';

@NgModule({
  imports: [
    CommonModule,
    TestRoutingModule
  ],
  declarations: [TestRoutingComponent]
})
export class TestModuleModule { }
