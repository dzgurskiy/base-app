import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestRoutingComponent } from './components/test-routing/test-routing.component';

const routes: Routes = [{
  path: '',
  component: TestRoutingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }