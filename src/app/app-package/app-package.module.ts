import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule } from '@angular/material';

import { AppPackageRoutingModule } from './app-package-routing.module';

import { SettingsService } from './services/settings.service';

import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { MainComponent } from './components/main/main.component';

export * from './services/settings.service';

@NgModule({
  imports: [
    CommonModule,
    AppPackageRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [
    HeaderComponent,
    ContentComponent,
    MainComponent
  ],
  exports: [
    MainComponent
  ],
  providers: [
    SettingsService
  ]
})
export class AppPackageModule { }
