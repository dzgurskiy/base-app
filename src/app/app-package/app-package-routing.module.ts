import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestModuleModule } from './modules/test-module/test-module.module';

export function TestModuleModuleFn() {
  return TestModuleModule;
}

const routes: Routes = [{
  path: 'test',
  loadChildren: TestModuleModuleFn
}, {
  path: '',
  pathMatch: 'full',
  redirectTo: '/test'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppPackageRoutingModule { }
