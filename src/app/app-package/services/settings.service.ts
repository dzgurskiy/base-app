import { Injectable } from '@angular/core';

@Injectable()
export class SettingsService {

  isShowMiniFabButtons = true;
  isShowLeftMenu = true;
  isShowRightMenu = true;

  constructor() { }

}
