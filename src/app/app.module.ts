import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppPackageModule } from './app-package/app-package.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppPackageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
